import os
from json import JSONDecodeError

import pytest

import homework
from homework import take_from_list, calculate


@pytest.mark.parametrize("li,indices,res", [([0], 0, [0]), (list(range(10)), [0, 1, 2], [0, 1, 2]), ("Ala ma kota", [0, 2, 5, -1], ['A', 'a', 'a', 'a'])])
def test_take_from_list(li, indices, res):
    assert take_from_list(li, indices) == res


@pytest.mark.parametrize("indices", [[1, 1.5], "kot", [2, 3, "koty"], None, [None], [[]], [{}], {}])
def test_take_wrong_arguments1(indices):
    with pytest.raises(ValueError):
        result = take_from_list([], indices)


@pytest.mark.parametrize("li", [1, 2.3, None])
def test_take_wrong_arguments2(li):
    with pytest.raises(TypeError):
        result = take_from_list(li, [-1, 2])


@pytest.mark.parametrize("indices", [[1, 2, 123], -300, [-1, 300]])
def test_take_wrong_indices(indices):
    with pytest.raises(IndexError):
        result = take_from_list([], indices)


def test_take_from_empty():
    with pytest.raises(IndexError):
        take_from_list([], 2)


def test_take_nothing():
    assert take_from_list([1, 2, 3], []) == []


def test_calculate_no_file(tmp_path):
    d = tmp_path / "sub"
    d.mkdir()
    with pytest.raises(FileNotFoundError):
        calculate(d / "in.json", d / "out.json")


def test_calculate(tmp_path, monkeypatch):
    def mock_take(li, indices):
        return li

    d = tmp_path / "sub"
    d.mkdir()
    monkeypatch.setattr(homework, "take_from_list", mock_take)
    with open(d / "in.json", "w") as fp:
        fp.write('{"list": [1,2,3], "indices": [0,1]}')
    calculate(d / "in.json", d / "out.json")
    assert os.path.exists(d / "out.json")


def test_calculate_wrong_file(tmp_path):
    d = tmp_path / "sub"
    d.mkdir()
    with open(d / "in.json", "w") as fp:
        fp.write('ala ma kota')
    with pytest.raises(JSONDecodeError):
        calculate(d / "in.json", d / "out.json")
